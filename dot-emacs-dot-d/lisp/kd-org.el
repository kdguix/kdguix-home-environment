(make-face 'kd/org-mode-face)
(set-face-attribute 'kd/org-mode-face nil :font "Fira Code" :height (+ kd/default-font-size 20))

(defun org-mode-setup ()
  (org-indent-mode 1))

(add-hook 'org-mode-hook #'org-mode-setup)
(add-hook 'org-mode-hook (lambda ()
			   (buffer-face-set 'kd/org-mode-face)))

(setq org-hide-emphasis-markers t)

(font-lock-add-keywords 'org-mode
                        '(("^ *\\([-]\\) "
                           (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
