(require 'toml-mode)
(add-to-list 'auto-mode-alist '("\\.toml\\'" . toml-mode))
;; (add-hook 'toml-mode-hook 'lsp-deferred)
