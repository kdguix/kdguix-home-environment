(require 'consult)

(defun kd/get-project-root ()
  (when (fboundp 'projectile-project-root)
    (projectile-project-root)))

(setq consult-project-root-function #'kd/get-project-root)
;; (setq completion-in-region-function #'consult-completion-in-region)

(kd/leader-keys
 "c" '(:ignore t :which-key "Consult keymaps")
 "cs" '(consult-line :which-key "Consule find line")
 "cl" '(consult-imenu :which-key "Consult IMenu")
 "cj" '(persp-switch-to-buffer* :which-key "Switch to buffer"))

(define-key 'minibuffer-local-map (kbd "C-r") 'consult-history)
