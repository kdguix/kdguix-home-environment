(require 'projectile)
(projectile-mode 1)

(kd/leader-keys
 "p" '(:ignore t :which-key "Projectile keymaps")
 "pf" '(:ignore t :which-key "Files")
 "pff" '(projectile-find-file :which-key "Find")
 "pp" '(projectile-command-map :which-key "Command map"))
