(setq-default evil-want-keybinding nil)

(require 'evil)
(evil-mode 1)

(require 'evil-escape)
(evil-escape-mode 1)
(setq-default evil-escape-key-sequence "jk")
(setq-default evil-escape-delay 0.1) ; This is a default value

(require 'evil-collection)

(with-eval-after-load 'dired
  (evil-collection-init 'dired))

(with-eval-after-load 'calendar
  (evil-collection-init 'calendar))

(with-eval-after-load 'org
  (evil-collection-init 'org))

(with-eval-after-load 'magit
  (evil-collection-init 'magit))

(with-eval-after-load 'snake
  (evil-collection-init 'snake))

(with-eval-after-load 'tetris
  (evil-collection-init 'tetris))

(with-eval-after-load 'company
  (evil-collection-init 'company))

(require 'evil-nerd-commenter)
(global-set-key (kbd "M-/") 'evilnc-comment-or-uncomment-lines)
