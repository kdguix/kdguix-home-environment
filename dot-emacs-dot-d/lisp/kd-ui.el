;; -*- coding: utf-8; lexical-binding: t; -*-

;; Load custom theme
(require 'solarized-theme)
(require 'jazz-theme)
(require 'doom-themes)
;; (load-theme 'doom-palenight t)
(load-theme 'jazz t)

;; Use mood-line instead of my custom

;; @see http://emacs-fu.blogspot.com/2011/08/customizing-mode-line.html
;; @see http://www.delorie.com/gnu/docs/elisp-manual-21/elisp_360.html
;; use setq-default to set it for /all/ modes
;; (defvar my-extra-mode-line-info '()
;;   "Extra info displayed at the end of the mode line.")

;; (setq-default mode-line-format
;;   (list
;;     ;; the buffer name
;;    "%b"

;;     ;; line and column
;;    ;; '%02' to set to 2 chars at least; prevents flickering
;;     "(%02l,%01c)"

;;     "["

;;     "%m " ; major mode name

;;     ;; buffer file encoding
;;     '(:eval (let ((sys (coding-system-plist buffer-file-coding-system)))
;;               (if (memq (plist-get sys :category)
;;                         '(coding-category-undecided coding-category-utf-8))
;;                   "UTF-8"
;;                 (upcase (symbol-name (plist-get sys :name))))))
;;     " "

;;     ;; insert vs overwrite mode
;;     '(:eval (propertize (if overwrite-mode "O" "I")
;;               'face nil
;;               'help-echo (concat "Buffer is in "
;;                            (if overwrite-mode "overwrite" "insert") " mode")))

;;     ;; was this buffer modified since the last save?
;;     '(:eval (and (buffer-modified-p)
;;                  (propertize "M"
;;                              'face nil
;;                              'help-echo "Buffer has been modified")))

;;     ;; is this buffer read-only?
;;     '(:eval (and buffer-read-only
;;                  (propertize "R" 'face nil 'help-echo "Buffer is read-only")))
;;     "] "

;;     ;; `global-mode-string' is useful because `org-timer-set-timer' uses it
;;     "%M"

;;     '(:eval my-extra-mode-line-info)

;;     " %-" ;; fill with '-'
;;     ))

(with-eval-after-load 'time
  ;; Donot show system load in mode line
  (setq display-time-default-load-average nil)
  ;; By default, the file in environment variable MAIL is checked
  ;; It's "/var/mail/my-username"
  ;; I set `display-time-mail-function' to display NO mail notification in mode line
  (setq display-time-mail-function (lambda () nil)))

(setq-default frame-title-format "GNU Emacs: %b")

;; You will most likely need to adjust this font size for your system!
(defvar kd/default-font-size 90)
(defvar kd/default-variable-font-size 90)

;; Make frame transparency overridable
(defvar kd/frame-transparency '(100 . 100))

;; Set frame transparency
(set-frame-parameter (selected-frame) 'alpha kd/frame-transparency)
(add-to-list 'default-frame-alist `(alpha . ,kd/frame-transparency))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(set-face-attribute 'default nil :font "Fira Code" :height kd/default-font-size)

;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "Fira Code" :height kd/default-font-size)

;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "Cantarell" :height kd/default-variable-font-size :weight 'regular)

(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

;; (require 'mood-line)

;; ;; The default set of glyphs:
;; ;;   * myModifiedFile.js  Replace*3                 + main  Javascript  ! Issues: 2
;; ;(setq mood-line-glyph-alist mood-line-glyphs-ascii)

;; ;; A set of Fira Code-compatible Unicode glyphs:
;; ;;   ● myModifiedFile.js  Replace×3                 + main  JavaScript  → Issues: 2
;; (setq mood-line-glyph-alist mood-line-glyphs-fira-code)

;; ;; A set of Unicode glyphs:
;; ;;   ● myModifiedFile.js  Replace✕3                 🞤 main  JavaScript  ⚑ Issues: 2
;; ;(setq mood-line-glyph-alist mood-line-glyphs-unicode)

;; (mood-line-mode)
