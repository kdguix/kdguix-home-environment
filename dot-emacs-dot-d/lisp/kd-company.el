
(defun kd/company-mode-setup ()
  (require 'company)
  (company-mode)
  (define-key company-active-map (kbd "<tab>") 'company-complete-selection)
  (setq company-minimum-prefix-length 1)
  (setq company-idle-delay 0.0))

(add-hook 'prog-mode-hook #'kd/company-mode-setup)
