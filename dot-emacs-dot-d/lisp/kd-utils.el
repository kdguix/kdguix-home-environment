(setq
 backup-by-copying t      ; don't clobber symlinks
 backup-directory-alist
 '(("." . "~/.kdmacs-saves/"))    ; don't litter my fs tree
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)

;; Scrolling settings
(setq scroll-step               1)
(setq scroll-margin            10)
(setq scroll-conservatively 10000)

(require 'which-key)
(which-key-mode)
(setq which-key-idle-delay 1)

(require 'hydra)

(require 'general)

(general-create-definer kd/leader-keys
			:keymaps '(normal insert visual emacs)
			:prefix "SPC"
			:global-prefix "C-SPC")
