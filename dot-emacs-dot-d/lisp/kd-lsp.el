(defun efs/lsp-mode-setup ()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode))

(require 'lsp-mode)
(add-hook 'lsp-mode-hook #'efs/lsp-mode-setup)
(add-hook 'lsp-mode-hook 'company-mode)
(setq lsp-keymap-prefix "C-l")
(lsp-enable-which-key-integration t)

(require 'lsp-ui)
(add-hook 'lsp-mode-hook 'lsp-ui-mode)
(setq lsp-ui-doc-position 'bottom)

(require 'lsp-treemacs)
