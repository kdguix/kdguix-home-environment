(make-face 'kd/dired-mode-face)
(set-face-attribute 'kd/dired-mode-face nil :font "Fira Code" :height (+ kd/default-font-size 20))

(add-hook 'dired-mode-hook (lambda ()
			     (buffer-face-set 'kd/dired-mode-face)))
