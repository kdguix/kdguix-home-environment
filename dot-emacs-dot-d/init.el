
;;; Commentary:
;;
;; KDMacs - Klementiev Dmitry eMacs
;;
;;; Code:

;; Remove warnings at startup
(setq warning-minimum-level :emergency)

(defun display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
	   (format "%.2f seconds"
		   (float-time
		    (time-subtract after-init-time before-init-time)))
	   gcs-done))

(add-hook 'emacs-startup-hook #'display-startup-time)

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(let* ((minver "28.2"))
  (when (version< emacs-version minver)
    (error "Emacs v%s or higher is required" minver)))

(setq user-init-file (or load-file-name (buffer-file-name)))
(setq user-emacs-directory (file-name-directory user-init-file))

(defvar my-debug nil "Enable debug mode.")

(setq *is-a-mac* (eq system-type 'darwin))
(setq *win64* (eq system-type 'windows-nt))
(setq *cygwin* (eq system-type 'cygwin) )
(setq *linux* (or (eq system-type 'gnu/linux) (eq system-type 'linux)) )
(setq *unix* (or *linux* (eq system-type 'usg-unix-v) (eq system-type 'berkeley-unix)) )
(setq *emacs28* (>= emacs-major-version 28))

;; don't GC during startup to save time
(unless (bound-and-true-p my-computer-has-smaller-memory-p)
  (setq gc-cons-percentage 0.6)
  (setq gc-cons-threshold most-positive-fixnum))

(defconst my-emacs-d (file-name-as-directory user-emacs-directory)
  "Directory of emacs.d.")

(defconst my-site-lisp-dir (concat my-emacs-d "site-lisp")
  "Directory of site-lisp.")

(defconst my-lisp-dir (concat my-emacs-d "lisp")
  "Directory of personal configuration.")

(defun require-init (pkg &optional maybe-disabled)
  "Load PKG if MAYBE-DISABLED is nil or it's nil but start up in normal slowly."
  (when (not maybe-disabled)
    (load (file-truename (format "%s/%s" my-lisp-dir pkg)) t t)))

(defun require-language (pkg &optional maybe-disabled)
  (when (not maybe-disabled)
    (load (file-truename (format "%s/kd-languages/%s" my-lisp-dir pkg)) t t)))

(defun my-add-subdirs-to-load-path (lisp-dir)
  "Add sub-directories under LISP-DIR into `load-path'."
  (let* ((default-directory lisp-dir))
    (setq load-path
          (append
           (delq nil
                 (mapcar (lambda (dir)
                           (unless (string-match "^\\." dir)
                             (expand-file-name dir)))
                         (directory-files lisp-dir)))
           load-path))))

(defun lazy-require-init (hook-with-modules)
  (catch 'h-w-m-loop
    (when (daemonp)
      (dolist (h-w-m hook-with-modules)
	(dolist (module (cdr h-w-m))
	  (module)))
      (throw 'h-w-m-loop t))

    (dolist (h-w-m hook-with-modules)
      (defconst hook (car h-w-m))
      (dolist (module (cdr h-w-m))
	(add-hook hook module)))))

;; @see https://www.reddit.com/r/emacs/comments/3kqt6e/2_easy_little_known_steps_to_speed_up_emacs_start/
;; Normally file-name-handler-alist is set to
;; (("\\`/[^/]*\\'" . tramp-completion-file-name-handler)
;; ("\\`/[^/|:][^/|]*:" . tramp-file-name-handler)
;; ("\\`/:" . file-name-non-special))
;; Which means on every .el and .elc file loaded during start up, it has to runs those regexps against the filename.
(let* ((file-name-handler-alist nil)
       (gc-cons-threshold most-positive-fixnum))

  ;; @see https://github.com/ajgrf/on.el
  (require 'on)

  (require-init 'kd-evil)
  (require-init 'kd-utils)
  (require-init 'kd-smartparens)

  (defconst lazy-modules '((on-init-ui-hook . ((lambda () (require-init 'kd-ui))))
			   (on-first-file-hook . ((lambda () (require-init 'kd-projectile))
						  (lambda () (require-init 'kd-git))
						  (lambda () (require-init 'kd-org))
						  (lambda () (require-init 'kd-lsp))))
			   (on-first-input-hook . ((lambda () (require-init 'kd-consult))
						   (lambda () (require-init 'kd-company))
						   (lambda () (require-init 'kd-vertico))
						   (lambda () (require-init 'kd-dired))))))

  (lazy-require-init lazy-modules)

  (defconst languages '(rust
			toml))
  (add-to-list 'load-path (concat my-lisp-dir "/kd-languages"))
  (dolist (lang languages)
    (autoload (intern (format "%s-mode" lang)) (format "kdl-%s" lang)))
  )
