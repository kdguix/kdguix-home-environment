(use-modules (gnu home)
	     (gnu home services)
	     (gnu services)
	     (guix build-system emacs)
	     (guix packages)
	     (guix licenses)
	     (guix git-download)
	     (guix gexp)
	     (guix download)
	     (guix build-system gnu)
	     (gnu packages emacs)
	     (gnu packages emacs-xyz)
	     (gnu packages web-browsers)
	     (gnu packages fonts)
	     (gnu packages base)
	     (gnu packages elf)
	     (gnu packages python)
	     (gnu packages python-xyz)
	     (gnu packages glib)
	     (gnu packages tree-sitter)
	     (gnu packages crates-io)
	     (ice-9 match)
	     (gnu packages bittorrent))

(define-public rustup
  (package
    (name "rustup")
    (version "1.27.1")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://static.rust-lang.org/rustup/archive/"
                            version "/x86_64-unknown-linux-gnu/rustup-init"))
        (sha256
         (base32 "TODO: Вставьте SHA256 сумму rustup-init здесь")))
      (file-name (string-append name "-" version ".tar.gz")))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let ((rustup-init (assoc-ref %build-inputs "rustup-init"))
               (rust-installer (string-append %output "/bin/rustup-init")))
           (install-file rustup-init rust-installer)
           #t))
       #:pre-inst-env
       (string-append "PATH=$PATH:" %bootstrap-bin)
       #:tests? #f
       #:tests-flags '("--test")
       #:phases
       (alist-cons-after
        'unpack 'bootstrap
        (lambda _
          (let ((bootstrap-bin (assoc-ref %build-inputs "rustup-init"))
                (rust-home (assoc-ref %build-inputs "rust-home")))
            (mkdir-p (string-append %output "/bin"))
            (mkdir-p (string-append %output "/etc"))
            (mkdir-p (string-append %output "/share/rustup"))
            (chmod  #o755 (string-append %output "/bin"))
            (chdir (string-append %output "/share/rustup")
                   (symlink (string-append rust-home "/share/doc/rust/html")
                            "doc")
                   (symlink (string-append rust-home "/lib/rustlib/src/rust/src")
                            "src")
                   (symlink (string-append rust-home "/share/man")
                            "man"))
            (invoke (string-append bootstrap-bin " --no-modify-path --default-toolchain none --profile minimal")
                    (string-append "--prefix=" %output)))))
     #:inputs
     `(("rustup-init" ,rustup-init)
       ("rust-home" ,rust-bootstrap))
     #:native-inputs
     `(("rust-bootstrap" ,rust-bootstrap))
     #:home-page "https://www.rustup.rs/"
     #:description "The Rust toolchain installer"))
    (license expat)))

(define-public rustc-bin
  (package
    (name "rustc")
    (version "1.55.0")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://static.rust-lang.org/dist/rust-"
                            version "-x86_64-unknown-linux-gnu.tar.gz"))
        (sha256
          (base32
            "0qimc0g2r3zgjw3x6i6i9b8v9n2d9g6b3c0n9jw0s2jx8r5j1ppj"))))
    (build-system gnu-build-system)
    (arguments
      `(#:tests? #f ; Нет тестов
        #:phases
        (modify-phases %standard-phases
          (add-after 'install 'remove-llvm-libs
            (lambda* (#:key outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (lib-dir (string-append out "/lib")))
                (delete-file-recursively lib-dir)
                #t))))))
    (home-page "https://www.rust-lang.org/")
    (synopsis "Rust programming language compiler")
    (description
      "The Rust compiler, also known as rustc, is the primary compiler for
the Rust programming language. It compiles Rust source code into executable
binaries.")
    (license (list license:asl2.0 license:expat))))

(define-public clippy-bin
  (package
    (name "clippy-bin")
    (version "1.70.0")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://static.rust-lang.org/dist/clippy-"
                            version "-x86_64-unknown-linux-gnu.tar.gz"))
        (sha256
          (base32
            "0qimc0g2r3zgjw3x6i6i9b8v9n2d9g6b3c0n9jw0s2jx8r5j1ppj"))))
    (build-system gnu-build-system)
    (arguments
      `(#:tests? #f ; Нет тестов
        #:phases
        (modify-phases %standard-phases
          (add-after 'install 'remove-llvm-libs
            (lambda* (#:key outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (lib-dir (string-append out "/lib")))
                (delete-file-recursively lib-dir)
                #t))))))
    (home-page "https://doc.rust-lang.org/cargo/")
    (synopsis "Rust package manager")
    (description
      "Cargo is the package manager and build tool for the Rust programming
language. It provides commands for building, testing, and managing Rust
projects and their dependencies.")
    (license (list license:asl2.0 license:expat))))

(define-public cargo-bin
  (package
    (name "cargo-bin")
    (version "1.55.0")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://static.rust-lang.org/dist/cargo-"
                            version "-x86_64-unknown-linux-gnu.tar.gz"))
        (sha256
          (base32
            "0qimc0g2r3zgjw3x6i6i9b8v9n2d9g6b3c0n9jw0s2jx8r5j1ppj"))))
    (build-system gnu-build-system)
    (arguments
      `(#:tests? #f ; Нет тестов
        #:phases
        (modify-phases %standard-phases
          (add-after 'install 'remove-llvm-libs
            (lambda* (#:key outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (lib-dir (string-append out "/lib")))
                (delete-file-recursively lib-dir)
                #t))))))
    (home-page "https://doc.rust-lang.org/cargo/")
    (synopsis "Rust package manager")
    (description
      "Cargo is the package manager and build tool for the Rust programming
language. It provides commands for building, testing, and managing Rust
projects and their dependencies.")
    (license (list license:asl2.0 license:expat))))

(define-public rustfmt-bin
  (package
    (name "rustfmt-bin")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/rust-lang/rustfmt/releases/download/"
                            "v" version "/rustfmt-" version "-x86_64-unknown-linux-gnu.tar.gz"))
        (sha256
          (base32
            "0q5x3kwpj5q5ax5mz0k1k2m8c7a9q9z3q8zv1j9zv2wq2jg6h9h9"))))
    (build-system gnu-build-system)
    (arguments
      `(#:tests? #f ; Нет тестов
        #:phases
        (modify-phases %standard-phases
          (add-after 'unpack 'rename-binary
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (bin (string-append out "/bin")))
                (rename-file (string-append out "/rustfmt") (string-append bin "/rustfmt"))
                #t))))))
    (home-page "https://github.com/rust-lang/rustfmt")
    (synopsis "Rust code formatter (precompiled binary)")
    (description
      "rustfmt is a tool for formatting Rust code according to style
guidelines. This package provides the precompiled binary release of rustfmt.")
    (license license:expat)))

(define-public rust-analyzer-bin
  (package
    (name "rust-analyzer-bin")
    (version "2021-07-12")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/rust-analyzer/rust-analyzer/releases/download/"
                            "2021-07-12/rust-analyzer-x86_64-unknown-linux-gnu.gz"))
        (sha256
          (base32
            "0h0r2p7q1l2k8xq4z5m3y1c6q9w5v0f3z9w1v9k8v6nq6j3p4z0z"))))
    (build-system gnu-build-system)
    (arguments
      `(#:phases
        (modify-phases %standard-phases
          (add-after 'unpack 'rename-binary
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (bin (string-append out "/bin")))
                (rename-file (string-append out "/rust-analyzer") (string-append bin "/rust-analyzer"))
                #t))))))
    (home-page "https://rust-analyzer.github.io/")
    (synopsis "Rust language server and analyzer (precompiled binary)")
    (description
      "rust-analyzer is a language server and static analysis tool for the
Rust programming language. This package provides the precompiled binary release of rust-analyzer.")
    (license license:expat)))
