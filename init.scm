
;;; Commentary:
;;
;; Main Guix home configuration
;;
;; Currently I use it on my Ubuntu system
;;
;;; Code:

(use-modules (gnu home)
	     (gnu home services)
	     (gnu services)
	     (guix build-system emacs)
	     (guix packages)
	     (guix licenses)
	     (guix git-download)
	     (guix gexp)
	     (guix download)
	     (guix build-system gnu)
	     (gnu packages emacs)
	     (gnu packages emacs-xyz)
	     (gnu packages web-browsers)
	     (gnu packages fonts)
	     (gnu packages base)
	     (gnu packages elf)
	     (gnu packages python)
	     (gnu packages python-xyz)
	     (gnu packages glib)
	     (gnu packages tree-sitter)
	     (gnu packages crates-io)
	     (ice-9 match)
	     (gnu packages bittorrent))

(define-public lutris
  (package
   (name "lutris")
   (version "0.5.13")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/lutris/lutris")
		  (commit (string-append "v" version))))
	    (sha256 (base32 "178765i7y4wc2pgjk14hf4z4pmq884xbq6hrgqm14gi4n9ynpjvr"))))
   (build-system gnu-build-system)
   (arguments
    `(#:phases
      (modify-phases %standard-phases
		     (delete 'configure)
		     (delete 'check)
		     (delete 'build)
		     (replace 'install
			      (lambda* (#:key inputs outputs #:allow-other-keys)
				       (let* ((out (assoc-ref outputs "out"))
					      (src (assoc-ref inputs "source")))
					 (mkdir-p (string-append out "/bin"))
					 (invoke "ln" "-sf" (string-append src "/bin/lutris") (string-append out "/bin/lutris"))
					 )))
		     (replace 'patch-dot-desktop-files
			      (lambda* (#:key inputs outputs #:allow-other-keys)
				       (let* ((out (assoc-ref outputs "out"))
					      (src (assoc-ref inputs "source")))
					 (mkdir-p (string-append out "/share/applications"))
					 (map (lambda (icondir)
						(mkdir-p (string-append out "/share/icons/hicolor/" icondir "/apps"))
						(invoke "ln" "-sf" (string-append src "/share/icons/hicolor/" icondir "/apps/lutris.png")
						      (string-append out "/share/icons/hicolor/" icondir "/apps/lutris.png")))
					      '("16x16"
						"22x22"
						"24x24"
						"32x32"
						"48x48"
						"64x64"
						"128x128"
						"scalable"))
					 (invoke "ln"
						 "-sf"
						 (string-append src "/share/lutris")
						 (string-append out "/share/lutris"))
					 (mkdir-p (string-append out "/metainfo"))
					 (invoke "ln" "-sf" (string-append src "/metainfo/net.lutris.Lutris.metainfo.xml")
					       (string-append out "/metainfo/net.lutris.Lutris.metainfo.xml"))
					 (mkdir-p (string-append out "/man/man1"))
					 (invoke "ln" "-sf" (string-append src "/man/man1/lutris.1")
					       (string-append out "/man/man1/lutris.1"))
					 )))
		     )))
   (inputs `(("python" ,python)
	     ("python-pyyaml" ,python-pyyaml)
	     ("python-pygobject" ,python-pygobject)
	     ;; Add more dependencies
	     ;; @see https://github.com/lutris/lutris/blob/master/INSTALL.rst
	     ))
   (native-inputs `(("tar" ,tar)))
   (home-page "https://lutris.net")
   (synopsis "Open gaming platform for Linux")
   (description "Lutris is an open gaming platform for Linux that provides a unified interface for installing, configuring, and launching games.")
   (license gpl3+)))

(define-public firefox-bin
  (package
    (name "firefox-bin")
    (version "115.0.2")
    (source (origin
              (method url-fetch)
              (uri
	       (string-append
		"https://download-installer.cdn.mozilla.net/pub/firefox/releases/115.0.2/linux-x86_64/en-US/firefox-"
		version
		".tar.bz2"))
              (sha256 (base32 "02ya8ilpplmdibpnfp8s1z67qilqg5prk2wf7xfaixldg57xykwy"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
	 (delete 'configure)
	 (delete 'check)
	 (delete 'install)
         (replace 'unpack
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (src (string-append out "/src"))
		    (bin (string-append out "/bin")))
               (mkdir-p src)
               (invoke "tar" "-xf" (assoc-ref inputs "source") "-C" src))))
	 (replace 'build
		  (lambda* (#:key inputs outputs #:allow-other-keys)
			   (let* ((out (assoc-ref outputs "out"))
				  (src (string-append out "/src"))
				  (bin (string-append out "/bin"))
				  (binary-files (list "")))
			     (mkdir-p bin)
			     (invoke "ln" "-sf" (string-append src "/firefox/firefox") (string-append bin "/"))
			     )))
	 (replace 'patch-dot-desktop-files
		    (lambda* (#:key inputs outputs #:allow-other-keys)
			     (let* ((out (assoc-ref outputs "out"))
				    (bin (string-append out "/bin/firefox"))
				    (icon (string-append out "/src/firefox/browser/chrome/icons/default/default128.png"))
				    (path (string-append out "/share/applications/")))
			       (mkdir-p path)
			       (with-output-to-file (string-append path "firefox.desktop")
				 (lambda ()
				   (display
				    (string-append
				     "[Desktop Entry]\n"
				     "Name=Firefox\n"
				     "Exec=" bin "\n"
				     "Icon=" icon "\n"
				     "Type=Application\n"))))
			       )))
	 )))
    (inputs `(("glibc" ,glibc)))
    (native-inputs `(("tar" ,tar)))
    (home-page "https://www.mozilla.org/firefox/")
    (synopsis "Web browser")
    (description "Mozilla Firefox is a free and open-source web browser developed by the Mozilla Foundation.")
    (license mpl2.0)))

(define-public emacs-jazz-theme
  (package
   (name "emacs-jazz-theme")
   (version "aa9a66d431df89b23e9f098ae3b86e65e75eee7b")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/donderom/jazz-theme")
		  (commit "aa9a66d431df89b23e9f098ae3b86e65e75eee7b")))
	    (sha256
	     (base32 "1mfawyw31abhaxxwxw7bpxg6pgagdiq8v81w7w59cj7q0swcd0lb"))))
   (build-system emacs-build-system)
   (home-page "https://github.com/donderom/jazz-theme")
   (description "Emacs jazz-theme")
   (synopsis "Emacs jazz-theme")
   (license gpl3+)))

(define-public emacs-eaf
  (package
   (name "emacs-eaf")
   (version "b06ec816c69ac576236474b73ae013da201a2a60")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/emacs-eaf/emacs-application-framework")
		  (commit "b06ec816c69ac576236474b73ae013da201a2a60")))
	    (sha256
	     (base32 "0p4qfjxjcw4lvwpvzfqzpjpcv36im492iw9chiifn5lr68zjnha5"))))
   (build-system emacs-build-system)
   (home-page "https://github.com/emacs-eaf/emacs-application-framework")
   (synopsis "Emacs Application Framework")
   (description "Emacs Application Framework")
   (license gpl3)))

(define-public kdmacs
  (package
   (inherit emacs)
   (name "kdmacs")
   (propagated-inputs
    `(("emacs-evil" ,emacs-evil)
      ("emacs-evil-collection" ,emacs-evil-collection)
      ("emacs-evil-escape" ,emacs-evil-escape)
      ("emacs-ement" ,emacs-ement)
      ("emacs-vterm" ,emacs-vterm)
      ("emacs-smartparens" ,emacs-smartparens)
      ("emacs-rainbow-delimiters" ,emacs-rainbow-delimiters)
      ("emacs-evil-nerd-commenter" ,emacs-evil-nerd-commenter)
      ("emacs-magit" ,emacs-magit)
      ("emacs-forge" ,emacs-forge)
      ("emacs-org" ,emacs-org)
      ("emacs-lsp-mode" ,emacs-lsp-mode)
      ("emacs-company" ,emacs-company)
      ("emacs-lsp-ui" ,emacs-lsp-ui)
      ("emacs-lsp-treemacs" ,emacs-lsp-treemacs)
      ;; ("emacs-lsp-ivy" ,emacs-lsp-ivy)
      ("emacs-dap-mode" ,emacs-dap-mode)
      ("emacs-rust-mode" ,emacs-rust-mode)
      ("emacs-toml-mode" ,emacs-toml-mode)
      ("emacs-consult" ,emacs-consult)
      ("emacs-vertico" ,emacs-vertico)
      ("emacs-bind-map" ,emacs-bind-map)
      ("emacs-perspective" ,emacs-perspective)
      ("emacs-projectile" ,emacs-projectile)
      ("emacs-general" ,emacs-general)
      ("emacs-hydra" ,emacs-hydra)
      ("emacs-which-key" ,emacs-which-key)
      ("emacs-solarized-theme" ,emacs-solarized-theme)
      ("emacs-jazz-theme" ,emacs-jazz-theme)
      ("emacs-transmission" ,emacs-transmission)
      ("emacs-on" ,emacs-on)
      ("emacs-doom-themes" ,emacs-doom-themes)))
   (replacement emacs)))

(define-public kdmacs-config
  (simple-service 'dot-emacs-dot-d
		  home-files-service-type
		  `((".emacs.d/lisp"
		     ,(local-file
		       "./dot-emacs-dot-d/lisp"
		       #:recursive? #t))
		    (".emacs.d/init.el"
		     ,(local-file
		       "./dot-emacs-dot-d/init.el"))
		    (".emacs.d/early-init.el"
		     ,(local-file
		       "./dot-emacs-dot-d/early-init.el")))))

(define-public gitconfig
  (simple-service 'dot-gitconfig
		  home-files-service-type
		  `((".gitconfig"
		    ,(local-file
		      "./dot-gitconfig")))))

(define-public dot-config/guix
  (simple-service 'dot-config/guix
		  home-files-service-type
		  `((".config/guix/channels.scm"
		     ,(local-file
		       "./channels.scm")))))

(home-environment
 (packages (list kdmacs
		 transmission
		 ;; nyxt
		 firefox-bin
		 ;; lutris-bin
		 ;; rustc-bin
		 ;; cargo-bin
		 ;; rustfmt-bin
		 ;; rust-analyzer-bin
		 tree-sitter
		 font-abattis-cantarell
		 font-fira-code))

 (services (list
	    dot-config/guix
	    kdmacs-config
	    gitconfig)))
