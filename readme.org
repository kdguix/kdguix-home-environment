#+title: My Guix Home Configuration
#+author: Klementiev Dmitry

* Why Fedora with Guix instead of GuixSD

Because I need some packages which not packaged in GuixSD yet.

* Packages which I need in GuixSD

- [-] Lutris - In alpha testing now. <3
- [X] Firefox (Binary) - I have already written version of binary firefox package. It's just install firefox binaries
  from website. Copy =firefox= to =$out/bin= and create =firefox.desktop= in =$out/share/applications=. It's may be
  slower than version from Debian/Arch repositories.
- [0%] Rust toolchain
  - [ ] =rustc-bin=
  - [ ] =cargo-bin=
  - [ ] =rustfmt-bin=
  - [ ] =rust-analyzer-bin=
  - [ ] =clippy-bin=

* Unnecessary package in GuixSD

- [ ] Discord - I can use Discord in browser
- [ ] Element (Matrix client) - I can use Element in Browser
- [ ] LLaMA ([[https://github.com/ggerganov/llama.cpp][llama.cpp]])
